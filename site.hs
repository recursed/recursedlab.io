--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Data.Monoid (mappend)
import           Hakyll
--------------------------------------------------------------------------------
-- compiles a subfolder inside ./contents/
compileSub :: String -> Rules ()
compileSub str = do
      let url = "contents/" ++ str ++ "/*"
      let pt = fromGlob url
      match pt $ version "titleLine" $ do
            route $ setExtension "html"
            compile $ do
                  pandocCompiler
                        >>= relativizeUrls
      match pt $ do
        route $ setExtension "html"
        compile $ do
              tops <- loadAll ("contents/top/*" .&&. hasVersion "titleLine")
              world <- loadAll ("contents/world/*" .&&. hasVersion "titleLine")
              places <- loadAll ("contents/places/*" .&&. hasVersion "titleLine")
              gameDesigns <- loadAll ("contents/game_design/*" .&&. hasVersion "titleLine")
              others <- loadAll ("contents/others/*" .&&. hasVersion "titleLine")
              let indexCtx =
                      listField "tops" indexCtx (return tops) `mappend`
                      listField "world" indexCtx (return world) `mappend`
                      listField "places" indexCtx (return places) `mappend`
                      listField "gameDesigns" indexCtx (return gameDesigns) `mappend`
                      listField "others" indexCtx (return others) `mappend`
                      defaultContext
              pandocCompiler
                    >>= loadAndApplyTemplate "templates/default.html" indexCtx
                    >>= relativizeUrls

main :: IO ()
main = hakyll $ do
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    compileSub "top"
    compileSub "world"
    compileSub "places"
    compileSub "game_design"
    compileSub "others"


    match "index.html" $ do
        route idRoute
        compile $ do
            tops <- loadAll ("contents/top/*" .&&. hasVersion "titleLine")
            world <- loadAll ("contents/world/*" .&&. hasVersion "titleLine")
            places <- loadAll ("contents/places/*" .&&. hasVersion "titleLine")
            gameDesigns <- loadAll ("contents/game_design/*" .&&. hasVersion "titleLine")
            others <- loadAll ("contents/others/*" .&&. hasVersion "titleLine")
            let indexCtx =
                    listField "tops" indexCtx (return tops) `mappend`
                    listField "world" indexCtx (return world) `mappend`
                    listField "places" indexCtx (return places) `mappend`
                    listField "gameDesigns" indexCtx (return gameDesigns) `mappend`
                    listField "others" indexCtx (return others) `mappend`
                    defaultContext
            getResourceBody
                >>= applyAsTemplate indexCtx
                >>= loadAndApplyTemplate "templates/default.html" indexCtx
                >>= relativizeUrls

    match "templates/*" $ compile templateCompiler

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
    dateField "date" "%B %e, %Y" `mappend`
    defaultContext
