Recursed Wiki
=======================

The Hakyll source of game project Recursed's wiki.

## Build Instructions

```bash
stack setup
stack build
stack exec recursed-wiki clean
stack exec recursed-wiki build
```

## Contributing

We welcome suggestions in any form, preferably via "issues".
We do not need outside contributors at this moment.

## License

The code is licensed under BSD 3 Clause license.
The contents is licensed under CC-BC-SA.
